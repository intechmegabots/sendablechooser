#include "WPILib.h"

class Robot: public IterativeRobot
{
public:
    enum AutonomousModes
    {
        AUTO_MODE_1,
        AUTO_MODE_2
    };

    enum TeleopModes
    {
        TELEOP_MODE_1,
        TELEOP_MODE_2
    };

private:
    SendableChooser *pAutonomousModeSelector;
    SendableChooser *pTeleopModeSelector;
    LiveWindow *pLiveWindow;

public:
    Robot()
    {
        pLiveWindow = NULL;
        pAutonomousModeSelector = NULL;
        pTeleopModeSelector = NULL;
    }

private:
    void RobotInit()
    {
        pLiveWindow = LiveWindow::GetInstance();

        SmartDashboard::init();
        NetworkTable* pNetworkTable = NetworkTable::GetTable("SmartDashboard");

        // Set up selector for different Autonomous Modes
        pAutonomousModeSelector = new SendableChooser();
        pAutonomousModeSelector->InitTable(pNetworkTable);
        pAutonomousModeSelector->AddDefault( "Auto Mode 1", (void*)AUTO_MODE_1);
        pAutonomousModeSelector->AddObject("Auto Mode 2", (void*)AUTO_MODE_2);
        SmartDashboard::PutData("Autonomous Mode Chooser", pAutonomousModeSelector);

        // Set up selector for different Teleop Modes
        pTeleopModeSelector = new SendableChooser();
        pTeleopModeSelector->InitTable(pNetworkTable);
        pTeleopModeSelector->AddDefault("Teleop Mode 1", (void*)TELEOP_MODE_1);
        pTeleopModeSelector->AddObject("Teleop Mode 2", (void*)TELEOP_MODE_2);
        SmartDashboard::PutData("Teleop Mode Chooser", pTeleopModeSelector);
}

    void AutonomousInit()
    {
        int mode = (int)pAutonomousModeSelector->GetSelected();
        switch( mode )
        {
        case AUTO_MODE_1:
            AutoMode1Init();
            break;
        case AUTO_MODE_2:
            AutoMode2Init();
            break;
        default:
            break;
        }
    }

    void AutonomousPeriodic()
    {
        int mode = (int)pAutonomousModeSelector->GetSelected();
        switch( mode )
        {
        case AUTO_MODE_1:
            AutoMode1Periodic();
            break;
        case AUTO_MODE_2:
            AutoMode2Periodic();
            break;
        default:
            break;
        }
    }

    void TeleopInit()
    {
        int mode = (int)pTeleopModeSelector->GetSelected();
        switch( mode )
        {
        case AUTO_MODE_1:
            TeleopMode1Init();
            break;
        case AUTO_MODE_2:
            TeleopMode2Init();
            break;
        default:
            break;
        }
    }

    void TeleopPeriodic()
    {
        int mode = (int)pTeleopModeSelector->GetSelected();
        switch( mode )
        {
        case TELEOP_MODE_1:
            TeleopMode1Periodic();
            break;
        case TELEOP_MODE_2:
            TeleopMode2Periodic();
            break;
        default:
            break;
        }
    }

    void TestInit()
    {
        std::cout << __FUNCTION__ << std::endl;
        pLiveWindow->Run();
    }

    void TestPeriodic()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void DisabledInit()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void DisabledPeriodic()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void AutoMode1Init()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void AutoMode1Periodic()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void AutoMode2Init()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void AutoMode2Periodic()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void TeleopMode1Init()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void TeleopMode1Periodic()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void TeleopMode2Init()
    {
        std::cout << __FUNCTION__ << std::endl;
    }

    void TeleopMode2Periodic()
    {
        std::cout << __FUNCTION__ << std::endl;
    }


};

START_ROBOT_CLASS(Robot);

