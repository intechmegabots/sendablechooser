# README #

This repository contains an example robot project derived from IterativeRobot.  It uses the SendableChooser object to set up the ability to select different Autonomous and Teleop modes.

The SmartDashbord folder contains a save.xml file which can be loaded into the Java Smart Dashboard to lay out dashboard elements that the robot uses.